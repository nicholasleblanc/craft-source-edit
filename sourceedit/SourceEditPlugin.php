<?php

/**
 * Source Edit by Nicholas LeBlanc
 *
 * @package   Source Edit
 * @author    Nicholas LeBlanc
 * @copyright Copyright (c) 2013, Nicholas LeBlanc
 * @link      http://nleblanc.com
 */

namespace Craft;

/**
 * Custom Plugin
 */
class SourceEditPlugin extends BasePlugin
{
    /**
     * Returns the plugin name
     *
     * @return null|string
     */
    public function getName()
    {
        return Craft::t('Source Edit');
    }

    /**
     * Returns the plugin version
     *
     * @return string
     */
    public function getVersion()
    {
        return '1.0';
    }

    /**
     * Returns the plugin developer
     *
     * @return string
     */
    public function getDeveloper()
    {
        return 'Nicholas LeBlanc';
    }

    /**
     * Returns the plugin developer URL
     *
     * @return string
     */
    public function getDeveloperUrl()
    {
        return 'http://nleblanc.com';
    }
}