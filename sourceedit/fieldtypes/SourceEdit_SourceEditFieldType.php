<?php

/**
 * Source Edit by Nicholas LeBlanc
 *
 * @package   Source Edit
 * @author    Nicholas LeBlanc
 * @copyright Copyright (c) 2013, Nicholas LeBlanc
 * @link      http://nleblanc.com
 */

namespace Craft;

/**
 * Custom Block Type
 */
class SourceEdit_SourceEditFieldType extends BaseFieldType
{
    /**
     * Returns the fieldtype name
     *
     * @return null|string
     */
    public function getName()
    {
        return Craft::t('Source Edit');
    }

    /**
     * Returns the content attribute config.
     *
     * @return mixed
     */
    public function defineContentAttribute()
    {
        return array(AttributeType::String, 'column' => ColumnType::Text);
    }

    /**
     * Defines settings for this fieldtype
     *
     * @return array
     */
    protected function defineSettings()
    {
        return array(
            'spaces'     => array(AttributeType::Bool, 'default' => 1),
            'indentUnit' => array(AttributeType::Number, 'default' => 4),
            'height'     => array(AttributeType::Number, 'default' => 300),
            'theme'      => array(AttributeType::Enum, 'values' => array(
                'clouds',
                'clouds_midnight',
                'cobalt',
                'crimson_editor',
                'dawn',
                'eclipse',
                'idle_fingers',
                'kr_theme',
                'merbivore',
                'merbivore_soft',
                'mono_industrial',
                'monokai',
                'pastel_on_dark',
                'solarized_dark',
                'solarized_light',
                'textmate',
                'twilight',
                'vibrant_ink'
            ), 'default' => 'monokai')
        );
    }

    /**
     * Returns the fieldtype's input HTML
     *
     * @param string $name
     * @param mixed  $value
     * @return string
     */
    public function getInputHtml($name, $value)
    {
        $id       = craft()->templates->formatInputId($name);
        $nsId     = craft()->templates->namespaceInputId($id);
        $settings = $this->getSettings();
        craft()->templates->includeJs('sourceEdit("' . $nsId . '", "' . $settings['theme'] . '", ' . $settings['spaces'] . ', ' . $settings['indentUnit'] . ');');

        return craft()->templates->render('sourceedit/input', array(
            'name'     => $name,
            'value'    => $value,
            'settings' => $settings,
            'id'       => $id
        ));
    }

    /**
     * Returns the fieldtype's setting HTML
     *
     * @return null|string
     */
    public function getSettingsHtml()
    {
        return craft()->templates->render('sourceedit/settings', array(
            'settings' => $this->getSettings()
        ));
    }
}